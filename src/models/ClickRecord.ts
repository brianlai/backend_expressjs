export type ClickRecord = { second: number, blueBtnClickCount: number; orangeBtnClickCount: number };

export type UserClickRecord = {
    userId: string,
    clickRecords: ClickRecord[]
};

export type ClickRecordStore = {
    [prop: string]: UserClickRecord
};

export type UserClickRecordInput = {
    userId: string
    clickRecord: ClickRecord
};
