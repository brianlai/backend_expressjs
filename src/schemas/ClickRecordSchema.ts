export const clickRecordSchema = `
type ClickRecord {
    second: Int!
    blueBtnClickCount: Int!
    orangeBtnClickCount: Int!
}

input ClickRecordInput {
    second: Int!
    blueBtnClickCount: Int!
    orangeBtnClickCount: Int!
}

type UserClickRecord {
    clickRecords: [ClickRecord!]!
    userId: String!
}

input UserClickRecordInput {
    userId: String!
    clickRecord: ClickRecordInput!
}

type Query {
    userClickRecord(userId: String): UserClickRecord
}

type Mutation{
    addUserClickRecord(userClickRecordInput: UserClickRecordInput): UserClickRecord
}

type Subscription {
    userClickRecord(userId: String): UserClickRecord
}
`;