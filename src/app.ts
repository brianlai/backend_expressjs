// const express = require("express");
import express, {Request, Response, NextFunction} from "express";
import {json} from "body-parser";
import cors from "cors";
import morgan from "morgan";

// import {graphqlHTTP} from "express-graphql"

import { execute, subscribe, buildSchema, GraphQLSchema, GraphQLSchemaConfig } from "graphql";
import { ApolloServer, gql }  from 'apollo-server-express';
import { createServer } from 'http';
import { PubSub } from 'graphql-subscriptions';
import { SubscriptionServer } from 'subscriptions-transport-ws';

import {UserClickRecord, ClickRecordStore, UserClickRecordInput} from "./models/ClickRecord"
import {clickRecordSchema} from "./schemas/ClickRecordSchema"

import { makeExecutableSchema } from '@graphql-tools/schema';

const PORT = 80

let clickRecordStore: ClickRecordStore = {}

const app = express();

//log incoming request with morgan
app.use(morgan("combined"))

// disable cors in production if expressjs backend and frontend share the same host
app.use(cors())

//these 2 route must be presented b4 "/*"
app.use("/app/index.bundle.js", express.static(__dirname + '/static/index.bundle.js'));
app.use("/app/main.css", express.static(__dirname + '/static/main.css'));

/**
 * graphql setup with apollo-server-express
 * https://www.apollographql.com/docs/graphql-subscriptions/express/
 */
app.use('/graphql', json())

//last fall back route to searve index.html (fallback for react router)
app.use("/*", express.static(__dirname + '/static/index.html'));

let typeDefs = gql`${clickRecordSchema}`

const pubsub = new PubSub()

type SubscriberType = () => Promise<void>
const subscribers: SubscriberType[] = [];
const onMessageUpdates = ((fn: SubscriberType) => subscribers.push(fn))

const resolvers = {
    Query: {
        userClickRecord: (_, args: {userId: string}) => {
            console.log("Hiting userClickRecord query handler")
            let userId = args.userId;

            if(userId in clickRecordStore) {
                return clickRecordStore[userId]
            } 
            
            return undefined
        },
    },
  
    Mutation: {
        addUserClickRecord: (_, { userClickRecordInput}: { userClickRecordInput: UserClickRecordInput  }) => {
            console.log("Hiting addUserClickRecord query handler")
            console.log(userClickRecordInput)

            if(!userClickRecordInput) {
                return
            }

            let userId = userClickRecordInput.userId;
            let userClickRecord: UserClickRecord|undefined = undefined 

            if(userId in clickRecordStore) {
                userClickRecord = clickRecordStore[userId]
            } else {
                userClickRecord = {
                    userId,
                    clickRecords: []
                }

                clickRecordStore[userId] = userClickRecord
            }

            if(userClickRecord) {
                userClickRecord.clickRecords.push(userClickRecordInput.clickRecord)
                console.log("clickRecordStore = " + JSON.stringify(clickRecordStore))
            }

            //push to client
            subscribers.forEach((fn: SubscriberType)=>fn())
            return userClickRecord
        }
    },
  
    Subscription: {
        userClickRecord: {
            subscribe: (_, {userId}: {userId: string}) => {
                console.log("Hitting subscribe()")
                console.log("userId", userId)

                const channel = Math.random().toString(36).slice(2, 15)
                onMessageUpdates(() => pubsub.publish(channel, {userClickRecord: clickRecordStore[userId]}))

                if(userId in clickRecordStore) {
                    setTimeout(() => pubsub.publish(channel, {userClickRecord: clickRecordStore[userId]}), 0)
                }
                
                return pubsub.asyncIterator(channel)
            }
        }
    }
  };

// may not need GraphQLSchema for ApolloServer() constructor, 
// can pass typeDefs, resolvers,
let mySchema = makeExecutableSchema({
    typeDefs,
    resolvers,
  });

const apolloServer = new ApolloServer({ schema: mySchema, playground: true});
apolloServer.applyMiddleware({ app });
const server = createServer(app);

// server.listen(PORT);

server.listen(PORT, () => {
    new SubscriptionServer({
      execute,
      subscribe,
      schema: mySchema,
    }, {
      server: server,
      path: '/graphql',
    });
});

/**
 * graphql server setup with express-graphql
 */
/* app.use("/graphql", graphqlHTTP({
    // schema: new GraphQLSchema(`` as GraphQLSchemaConfig),
    schema: buildSchema(clickRecordSchema),
    rootValue: {
        userClickRecord: (args: {userId: string}) => {
            console.log("Hiting userClickRecord query handler")
            let userId = args.userId;

            if(userId in clickRecordStore) {
                return clickRecordStore[userId]
            } 
            
            return undefined
        },
        addUserClickRecord: (args: {userClickRecordInput: UserClickRecordInput}) => {
            console.log("Hiting addUserClickRecord query handler")
            console.log(args)

            let userClickRecordInput = args.userClickRecordInput;
            let userId = userClickRecordInput.userId;
            let userClickRecord: UserClickRecord|undefined = undefined 

            if(userId in clickRecordStore) {
                userClickRecord = clickRecordStore[userId]
            } else {
                userClickRecord = {
                    userId,
                    clickRecords: []
                }

                clickRecordStore[userId] = userClickRecord
            }

            if(userClickRecord) {
                userClickRecord.clickRecords.push(userClickRecordInput.clickRecord)
                console.log("clickRecordStore = " + JSON.stringify(clickRecordStore))
            }

            return userClickRecord
        }
    },
    graphiql: true,
})) */


/* //last fall back route to searve index.html (fallback for react router)
app.use("/*", express.static(__dirname + '/static/index.html'));

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).json({message: err.message})
})

// app.listen(3000);
app.listen(80); */